import { useEffect, useState } from 'react'

import './App.css'

type SudokuElement = {
  value: number | null
  setBy: 'user' | 'program'
}

function App() {
  const [matrix, setMatrix] = useState<SudokuElement[][]>(
    Array.from({ length: 9 }, () =>
      Array.from({ length: 9 }, () => ({
        value: null,
        setBy: 'program'
      }))
    )
  )

  const getAllEmptyIndexes = (localMatrix: SudokuElement[][], ignoringIndex?: { row: number; col: number }) => {
    // array of cells by row
    const cellsByRow = localMatrix.map((row, rowIndex) =>
      row
        .map((_, colIndex) => ({
          row: rowIndex,
          col: colIndex
        }))
        .filter((cell) => localMatrix[cell.row][cell.col].value === null)
    )

    // array of cells by column
    const cellsByColumn = localMatrix.map((row, rowIndex) =>
      row
        .map((_, colIndex) => ({
          row: colIndex,
          col: rowIndex
        }))
        .filter((cell) => localMatrix[cell.row][cell.col].value === null)
    )

    // array of cells by square
    const cellsBySquare = Array.from({ length: 3 }, (_, rowIndex) =>
      Array.from({ length: 3 }, (_, colIndex) => {
        const i0 = rowIndex * 3
        const j0 = colIndex * 3

        return [
          { row: i0, col: j0 },
          { row: i0, col: j0 + 1 },
          { row: i0, col: j0 + 2 },
          { row: i0 + 1, col: j0 },
          { row: i0 + 1, col: j0 + 1 },
          { row: i0 + 1, col: j0 + 2 },
          { row: i0 + 2, col: j0 },
          { row: i0 + 2, col: j0 + 1 },
          { row: i0 + 2, col: j0 + 2 }
        ]
      })
    )
      .flat()
      .map((square) => square.filter((cell) => localMatrix[cell.row][cell.col].value === null))

    // console.log('Rows')
    // cellsByRow.forEach((row) => {
    //   console.log(row)
    // })

    // console.log('Columns')
    // cellsByColumn.forEach((column) => {
    //   console.log(column)
    // })

    // console.log('Squares')
    // cellsBySquare.forEach((square) => {
    //   console.log(square)
    // })

    const cells = [...cellsByRow, ...cellsByColumn, ...cellsBySquare].sort((a, b) => a.length - b.length)

    console.log('Cells', cells)

    const notRepeatedCells: [number, number][] = []

    for (const cellIndexes of cells) {
      for (const index of cellIndexes) {
        if (
          notRepeatedCells.some(([i, j]) => i === index.row && j === index.col) ||
          (ignoringIndex && ignoringIndex.row === index.row && ignoringIndex.col === index.col)
        )
          continue
        // console.log('adding', index)
        notRepeatedCells.push([index.row, index.col])
      }
    }

    return notRepeatedCells.reverse()
  }

  const updateItem = (i: number, j: number, value: string, setBy: SudokuElement['setBy'] = 'user') => {
    const newMatrix = [...matrix]

    const intValue = parseInt(value)
    const newValue = !isNaN(intValue) ? intValue : null
    const oldValue = newMatrix[i][j].value

    if (newValue === oldValue) return newMatrix

    newMatrix[i][j] = {
      value: newValue,
      setBy: newValue === null ? 'program' : setBy
    }

    // console.log('Updating item', i, j, 'from', oldValue, 'to', newValue, 'setBy', setBy)

    setMatrix(newMatrix)

    return newMatrix
  }

  const getCorrespondingValues = (localMatrix: SudokuElement[][], row: number, column: number) => {
    const rowValues = localMatrix[row]
    const columnValues = localMatrix.map((row) => row[column])

    const i0 = Math.floor(row / 3) * 3
    const j0 = Math.floor(column / 3) * 3

    const squareValues = localMatrix
      .slice(i0, i0 + 3)
      .map((row) => row.slice(j0, j0 + 3))
      .flat()

    return {
      rowValues,
      columnValues,
      squareValues
    }
  }

  const getPossibleValues = (localMatrix: SudokuElement[][], row: number, column: number) => {
    console.log('Getting possible values for', row, column)

    const { rowValues, columnValues, squareValues } = getCorrespondingValues(localMatrix, row, column)

    console.log(
      'Row values: ',
      rowValues
        .map((s) => s.value)
        .filter((v) => v)
        .join(', ')
    )
    console.log(
      'Column values: ',
      columnValues
        .map((s) => s.value)
        .filter((v) => v)
        .join(', ')
    )
    console.log(
      'Square values: ',
      squareValues
        .map((s) => s.value)
        .filter((v) => v)
        .join(', ')
    )

    const possibleValues = Array.from({ length: 9 }, (_, i) => i + 1)

    const usedValues = [...rowValues, ...columnValues, ...squareValues].map((cell) => cell.value)

    return possibleValues.filter((value) => !usedValues.includes(value))
  }

  const solveForIndex = async (
    row: number,
    column: number,
    localMatrix: SudokuElement[][]
  ): Promise<SudokuElement[][] | null> => {
    console.log('Solving...', row, column)

    const currentItem = localMatrix[row][column]
    const currentItemValue = currentItem.value

    const possibleValues =
      currentItem.setBy === 'program'
        ? getPossibleValues(localMatrix, row, column)
        : currentItem.value === null
        ? []
        : [currentItem.value as number]
    if (possibleValues.length === 0) {
      console.log("There's no possible values for this cell")
      return null
    }

    for (const [index, value] of possibleValues.entries()) {
      console.log(`Trying with possible value ${index}: ${value}`)
      const newMatrix = updateItem(row, column, value.toString(), currentItem.setBy)

      setMatrix(newMatrix)
      await new Promise((resolve) => setTimeout(resolve, 50))

      const followingEmptyIndexes = getAllEmptyIndexes(newMatrix, { row, col: column })

      const followingIndex = followingEmptyIndexes.pop()

      console.log('Following empty index', followingIndex)

      if (!followingIndex) {
        console.log('Empty index was undefined, terminating SOLUTION', followingIndex)
        return newMatrix
      }

      const [nextI, nextJ] = followingIndex

      const result = await solveForIndex(nextI, nextJ, newMatrix)

      if (result) {
        console.log('possible did work, moving on')
        return result
      } else {
        console.log("possible value didn't work, trying another one")
        const newMatrix = updateItem(
          row,
          column,
          currentItem.setBy == 'program' ? '' : currentItemValue?.toString() || '',
          currentItem.setBy
        )

        setMatrix(newMatrix)
        continue
      }
    }

    return null
  }

  const isInter = (i: number, j: number) => {
    const i0 = Math.floor(i / 3) * 3
    const j0 = Math.floor(j / 3) * 3

    if ((i0 % 2 == 0 && j0 % 2 == 0) || i0 == j0) return true
    return false
  }

  const isValid = (i: number, j: number) => {
    const { rowValues, columnValues, squareValues } = getCorrespondingValues(matrix, i, j)
    return [rowValues, columnValues, squareValues].some((values) => values.every((cell) => cell.value !== null))
  }

  const solve = async () => {
    const start = performance.now()
    const emptyIndexes = getAllEmptyIndexes(matrix)

    console.log('Gotten all empty indexes', emptyIndexes)

    const first = emptyIndexes.pop()
    if (!first) return

    const [x, y] = first
    await solveForIndex(x, y, matrix)
    const end = performance.now()
    // print Solved in ... ms if is less than 1000, seconds if less than 60000, minutes and seconds if more than 60000, hours, minutes and seconds if more than 3600000
    const time = end - start
    const timeString =
      time < 1000
        ? `${time.toFixed(0)} ms`
        : time < 60000
        ? `${(time / 1000).toFixed(0)} seconds`
        : time < 3600000
        ? `${Math.floor(time / 60000)} minutes and ${((time % 60000) / 1000).toFixed(0)} seconds`
        : `${Math.floor(time / 3600000)} hours, ${Math.floor((time % 3600000) / 60000)} minutes and ${(
            (time % 60000) /
            1000
          ).toFixed(0)} seconds`

    console.log(`Solved in ${timeString}`)
  }

  return (
    <>
      <h1>Mi Sudoku</h1>
      <button className="solve" onClick={() => solve()}>
        Solve
      </button>

      <div className="sudoku">
        {matrix.map((row, i) => (
          <div className="row" key={i}>
            {row.map((cell, j) => (
              <div
                className={['cell', ...(isInter(i, j) ? ['inter'] : []), ...(isValid(i, j) ? ['valid'] : [])].join(' ')}
                key={j}
              >
                <input
                  value={cell?.value?.toString() ?? ''}
                  onChange={(e) => updateItem(i, j, e.target.value)}
                  type="text"
                  maxLength={1}
                />
              </div>
            ))}
          </div>
        ))}
      </div>
    </>
  )
}

export default App
